This is Windows Hosted(MinGW-w64) gcc version 11.0.1 20210417 cross compiler of arm-none-eabi-g++ for Nitendo DS

I did some tweaks to GCC to make it work better on Nitendo.

Remove emergency heap in libsupc++. operator new will fail-fast instead of throw std::bad_alloc and global new is now noexcept.
Remove libstdc++ verbose terminate handler. std::terminate now calls __builin_trap()
New handler etc., will become noop.

You can still throw exceptions and the EH runtime bloat now reduces to 40kbs. That allows us to static linking libstdc++ freely.

Although iostream is still usable, i highly recommended not to use since iostream bloats binary for nearly 1 MB.

This also contains my own fast_io library (https://github.com/expnkx/fast_io) that can replace iostream without C++ runtime bloat and works perfectly with libgcc.
